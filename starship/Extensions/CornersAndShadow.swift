import UIKit
import Foundation

extension UIButton {
    func cornerRadius (_ radius: CGFloat = 15) {
        self.layer.cornerRadius = radius
}
    func dropShadow(_ radius: CGFloat = 15) {
            layer.masksToBounds = false
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOpacity = 0.5
            layer.shadowOffset = CGSize(width: 10, height: 10)
            layer.shadowRadius = 20
            layer.cornerRadius = radius
            
            layer.shadowPath = UIBezierPath(rect: bounds).cgPath
            layer.shouldRasterize = true
        }

}
extension UILabel {
    func cornerRadius (_ radius: CGFloat = 15) {
        self.layer.cornerRadius = radius
}
}
