import Foundation
import UIKit


class Settings: Codable {
    var difficulty: Int?
    var difficultyName: String?
    var enemyImage: String?
    var playersStarshipImage: String?
    var enemysStarshipImage: String?
    var fuelColour: String?
    var playerName: String?
    init(difficulty: Int, difficultyName: String, enemyImage:String, playersStarshipImage: String, enemysStarshipImage:String, fuelColour: String, playerName:String) {
        self.difficulty = difficulty
        self.difficultyName = difficultyName
        self.enemyImage = enemyImage
        self.playersStarshipImage = playersStarshipImage
        self.enemysStarshipImage = enemysStarshipImage
        self.fuelColour = fuelColour
        self.playerName = playerName
    }
    private enum CodingKeys: String, CodingKey {
        case difficulty
        case difficultyName
        case enemyImage
        case playersStarshipImage
        case enemysStarshipImage
        case fuelColour
        case playerName
    }
        required init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            difficulty = try container.decodeIfPresent(Int.self, forKey: .difficulty)
            difficultyName = try container.decodeIfPresent(String.self, forKey: .difficultyName)
            enemyImage = try container.decodeIfPresent(String.self, forKey: .enemyImage)
            playersStarshipImage = try container.decodeIfPresent(String.self, forKey: .playersStarshipImage)
            enemysStarshipImage = try container.decodeIfPresent(String.self, forKey: .enemysStarshipImage)
            fuelColour = try container.decodeIfPresent(String.self, forKey: .fuelColour)
            playerName = try container.decodeIfPresent(String.self, forKey: .playerName)
        }
        
        func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)

            try container.encode(self.difficulty, forKey: .difficulty)
            try container.encode(self.difficultyName, forKey: .difficultyName)
            try container.encode(self.enemyImage, forKey: .enemyImage)
            try container.encode(self.playersStarshipImage, forKey: .playersStarshipImage)
            try container.encode(self.enemysStarshipImage, forKey: .enemysStarshipImage)
            try container.encode(self.fuelColour, forKey: .fuelColour)
            try container.encode(self.playerName, forKey: .playerName)
        }
        
    }



