import UIKit

class HighscoresTableViewCell: UITableViewCell {

    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var player: UILabel!
    @IBOutlet weak var difficulty: UILabel!
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure (object: GameHighscores) {
        number.text = ""
        score.text = String(object.score!)
        player.text = object.playerName
        difficulty.text = object.difficultyName
        date.text = object.date
        
    }

}
