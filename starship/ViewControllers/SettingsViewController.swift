import UIKit


class SettingsViewController: UIViewController {
    @IBOutlet weak var diffLabel: UILabel!
    @IBOutlet weak var diffButtonOutlet: UIButton!
    @IBOutlet weak var backgroundSettings: UIView!
    @IBOutlet weak var playersSkinOne: UIImageView!
    @IBOutlet weak var playersSkinTwo: UIImageView!
    @IBOutlet weak var enemysSkinOne: UIImageView!
    @IBOutlet weak var enemysSkinTwo: UIImageView!
    @IBOutlet weak var playersNameTextField: UITextField!
    
    let background = UIImageView ()
    let backgroundTwo = UIImageView ()
    
    let speed: CGFloat = 30
    
    
    let enemyOne = UIImageView()
    var Setting = Settings(difficulty: 7, difficultyName: "Normal", enemyImage: "enemy", playersStarshipImage: "starship", enemysStarshipImage: "enemy", fuelColour: "green", playerName: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let Setting = UserDefaults.standard.value(Settings.self, forKey: Keys.mySetting.rawValue) else { return }
        self.diffLabel.text = Setting.difficultyName
        self.Setting = Setting
        
        createBackground()
        createBackgroundTwo()
        moveBackground()
        
        playersNameTextField.text = Setting.playerName
        
        if Setting.playersStarshipImage == "starship" {
            selectPlayersSkinOne()
        } else {
            selectPlayersSkinTwo()
        }
        
        if Setting.enemysStarshipImage == "enemy" {
            selectEnemySkinOne()
        } else {
            selectEnemySkinTwo()
        }
        
        let tapOnPlayersPlaneOne = UITapGestureRecognizer(target: self, action: #selector(selectPlayersSkinOne))
        let tapOnPlayersPlaneTwo = UITapGestureRecognizer(target: self, action: #selector(selectPlayersSkinTwo))
        self.playersSkinOne.addGestureRecognizer(tapOnPlayersPlaneOne)
        self.playersSkinTwo.addGestureRecognizer(tapOnPlayersPlaneTwo)
        
        let tapOnEnemysPlaneOne = UITapGestureRecognizer(target: self, action: #selector(selectEnemySkinOne))
        let tapOnEnemysPlaneTwo = UITapGestureRecognizer(target: self, action: #selector(selectEnemySkinTwo))
        self.enemysSkinOne.addGestureRecognizer(tapOnEnemysPlaneOne)
        self.enemysSkinTwo.addGestureRecognizer(tapOnEnemysPlaneTwo)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @objc func selectPlayersSkinOne () {
        playersSkinOne.layer.borderWidth = 5
        playersSkinOne.layer.borderColor = UIColor.systemPink.cgColor
        playersSkinTwo.layer.borderWidth = 0
        Setting.playersStarshipImage = "starship"
        
    }
    @objc func selectPlayersSkinTwo () {
        playersSkinTwo.layer.borderWidth = 5
        playersSkinTwo.layer.borderColor = UIColor.systemPink.cgColor
        playersSkinOne.layer.borderWidth = 0
        Setting.playersStarshipImage = "starshipTwo"
        
    }
    
    @objc func selectEnemySkinOne () {
        enemysSkinOne.layer.borderWidth = 5
        enemysSkinOne.layer.borderColor = UIColor.systemPink.cgColor
        enemysSkinTwo.layer.borderWidth = 0
        Setting.enemysStarshipImage = "enemy"
        
    }
    @objc func selectEnemySkinTwo () {
        enemysSkinTwo.layer.borderWidth = 5
        enemysSkinTwo.layer.borderColor = UIColor.systemPink.cgColor
        enemysSkinOne.layer.borderWidth = 0
        Setting.enemysStarshipImage = "enemyTwo"
        
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        //self.dismiss(animated: true, completion: nil)
        Setting.playerName = playersNameTextField.text
        UserDefaults.standard.set(encodable: Setting, forKey: Keys.mySetting.rawValue)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func createBackground() {
        
        background.frame = CGRect(x: 0,
                                  y: -50,
                                  width: self.view.frame.size.width,
                                  height: self.view.frame.size.height + 200)
        background.image = UIImage(named: "background_sky")
        background.contentMode = .scaleAspectFill
        self.backgroundSettings.addSubview(background)
        
        
    }
    func createBackgroundTwo() {
        
        backgroundTwo.frame = CGRect(x: 0,
                                     y: self.background.frame.origin.y - self.backgroundTwo.frame.size.height,
                                     width: self.view.frame.size.width,
                                     height: self.view.frame.size.height + 200)
        backgroundTwo.image = UIImage(named: "background_sky")
        backgroundTwo.contentMode = .scaleAspectFill
        self.backgroundSettings.addSubview(backgroundTwo)
        
        
    }
    
    
    func moveBackground() {
        let moveBackground = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (moveBackground) in
            if (self.background.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.background.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                self.backgroundTwo.frame.origin.y = self.background.frame.origin.y - self.backgroundTwo.frame.size.height
                self.moveBackgroundElements()
            } else if (self.backgroundTwo.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.backgroundTwo.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                self.background.frame.origin.y = self.backgroundTwo.frame.origin.y - self.background.frame.size.height
                self.moveBackgroundElements()
            } else {
                self.moveBackgroundElements()
            }
        }
        )}
    
    func moveBackgroundElements () {
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
            
            self.background.frame.origin.y += self.speed
            self.backgroundTwo.frame.origin.y += self.speed
            
        }
    }
    
    @IBAction func diffButtonPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose difficulty", message: "", preferredStyle: .alert)
        let hard = UIAlertAction(title: "Hard", style: .default) { (_) in
            self.Setting.difficulty = 2
            self.Setting.difficultyName = "Hard"
            self.diffLabel.text = "Hard"
        }
        let normal = UIAlertAction(title: "Normal", style: .default) { (_) in
            self.Setting.difficulty = 10
            self.Setting.difficultyName = "Normal"
            self.diffLabel.text = "Normal"
        }
        let easy = UIAlertAction(title: "Easy", style: .default) { (_) in
            self.Setting.difficulty = 20
            self.Setting.difficultyName = "Easy"
            self.diffLabel.text = "Easy"
        }
        alert.addAction(hard)
        alert.addAction(normal)
        alert.addAction(easy)
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension SettingsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

