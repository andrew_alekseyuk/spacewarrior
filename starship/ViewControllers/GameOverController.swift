import UIKit

class GameOverController: UIViewController {
    @IBOutlet weak var gameOverTest: UILabel!
    @IBOutlet weak var gameOverButton: UIButton!
    @IBOutlet weak var gameStatLabel: UILabel!
    @IBOutlet weak var congratsUserNameLabel: UILabel!
    @IBOutlet weak var resultsLabel: UILabel!
    @IBOutlet weak var resultsView: UIView!
    let gameOverText = "Game\nover"
    
    let background = UIImageView ()
    let backgroundTwo = UIImageView ()
    let leftBorder = UIImageView()
    let leftBorderTwo = UIImageView()
    let rightBorder = UIImageView()
    let rightBorderTwo = UIImageView()
    let speed: CGFloat = 30
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createBackground()
        createBackgroundTwo()
        moveBackground()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        gameOverTest.text = self.gameOverText
        gameOverTest.font = UIFont(name: "SpaceAge", size: 70)
        gameOverTest.textColor = .systemPink
        gameOverTest.textAlignment = .center
        
        gameOverButton.setTitle("Back to Menu", for: .normal)
        gameOverButton.titleLabel?.font = UIFont (name: "SpaceAge", size: 25)
        gameOverButton.cornerRadius()
        gameOverButton.dropShadow()
        
        guard var GameHighscoresArray = UserDefaults.standard.value([GameHighscores].self, forKey: Keys.scoreKey.rawValue) as? [GameHighscores] else {return}
        let lastIndex = GameHighscoresArray.endIndex - 1
        guard let playerName = GameHighscoresArray[lastIndex].playerName else {return}
        guard let score = GameHighscoresArray[lastIndex].score else {return}
        guard let difficultyName = GameHighscoresArray[lastIndex].difficultyName else {return}
        guard let date = GameHighscoresArray[lastIndex].date else {return}
        
        congratsUserNameLabel.text = "Congrats, \(playerName)"
        congratsUserNameLabel.font = UIFont(name: "SpaceAge", size: 20)
        congratsUserNameLabel.textColor = .systemPink
        resultsLabel.text = "Score:    \(score) \nDifficulty:    \(difficultyName) \nTimestamp:     \(date)"
        resultsLabel.font = UIFont(name: "SpaceAge", size: 20)
        resultsLabel.textColor = .systemPink
    }
    func createBackground() {
        
        background.frame = CGRect(x: 0,
                                  y: -50,
                                  width: self.view.frame.size.width,
                                  height: self.view.frame.size.height + 200)
        background.image = UIImage(named: "background_sky")
        background.contentMode = .scaleAspectFill
        self.view.addSubview(background)
        
        
        view.bringSubviewToFront(self.gameOverButton)
        view.bringSubviewToFront(self.gameOverTest)
        view.bringSubviewToFront(self.resultsView)
        
    }
    func createBackgroundTwo() {
        
        backgroundTwo.frame = CGRect(x: 0,
                                     y: self.background.frame.origin.y - self.backgroundTwo.frame.size.height,
                                     width: self.view.frame.size.width,
                                     height: self.view.frame.size.height + 200)
        backgroundTwo.image = UIImage(named: "background_sky")
        backgroundTwo.contentMode = .scaleAspectFill
        self.view.addSubview(backgroundTwo)
        
        
        view.bringSubviewToFront(self.gameOverButton)
        view.bringSubviewToFront(self.gameOverTest)
        view.bringSubviewToFront(self.resultsView)
    }
    
    
    func moveBackground() {
        let moveBackground = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (moveBackground) in
            if (self.background.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.background.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                self.backgroundTwo.frame.origin.y = self.background.frame.origin.y - self.backgroundTwo.frame.size.height
                self.moveBackgroundElements()
            } else if (self.backgroundTwo.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.backgroundTwo.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                self.background.frame.origin.y = self.backgroundTwo.frame.origin.y - self.background.frame.size.height
                self.moveBackgroundElements()
            } else {
                self.moveBackgroundElements()
            }
        }
        )}
    
    func moveBackgroundElements () {
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
            
            self.background.frame.origin.y += self.speed
            self.backgroundTwo.frame.origin.y += self.speed
        }
    }
    @IBAction func backToMenuButton(_ sender: UIButton) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
}
