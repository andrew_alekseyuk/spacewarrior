import UIKit

class HighscoresViewController: UIViewController {
    
    @IBOutlet weak var backgroundHighscores: UIView!
    @IBOutlet weak var highScoresTableView: UITableView!
    @IBOutlet weak var highscoresTableViewCell: HighscoresTableViewCell!
    
    var GameHighscoresArrayGlobal:[GameHighscores] = []
    let GameHighscoresArrayGlobalSorted:[GameHighscores] = []
    let background = UIImageView ()
    let backgroundTwo = UIImageView ()
    
    let speed: CGFloat = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let GameHighscoresArray = UserDefaults.standard.value([GameHighscores].self, forKey: Keys.scoreKey.rawValue) else {return}
        GameHighscoresArrayGlobal = GameHighscoresArray
        GameHighscoresArrayGlobal.sort(by: {$1.score! < $0.score!})
        
        createBackground()
        createBackgroundTwo()
        moveBackground()
            
        }
    
    func createBackground() {
        
        background.frame = CGRect(x: 0,
                                  y: -50,
                                  width: self.view.frame.size.width,
                                  height: self.view.frame.size.height + 200)
        background.image = UIImage(named: "background_sky")
        background.contentMode = .scaleAspectFill
        self.backgroundHighscores.addSubview(background)
        
        
    }
    func createBackgroundTwo() {
        
        backgroundTwo.frame = CGRect(x: 0,
                                     y: self.background.frame.origin.y - self.backgroundTwo.frame.size.height,
                                     width: self.view.frame.size.width,
                                     height: self.view.frame.size.height + 200)
        backgroundTwo.image = UIImage(named: "background_sky")
        backgroundTwo.contentMode = .scaleAspectFill
        self.backgroundHighscores.addSubview(backgroundTwo)
        
       
    }
    
    
    func moveBackground() {
        let moveBackground = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (moveBackground) in
            if (self.background.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.background.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                self.backgroundTwo.frame.origin.y = self.background.frame.origin.y - self.backgroundTwo.frame.size.height
                self.moveBackgroundElements()
            } else if (self.backgroundTwo.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.backgroundTwo.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                self.background.frame.origin.y = self.backgroundTwo.frame.origin.y - self.background.frame.size.height
                self.moveBackgroundElements()
            } else {
                self.moveBackgroundElements()
            }
        }
        )}
    
    func moveBackgroundElements () {
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
            
            self.background.frame.origin.y += self.speed
            self.backgroundTwo.frame.origin.y += self.speed
            
        }
    }
        
    
    @IBAction func clearResultsButton(_ sender: UIButton) {
        var GameHighscoresArray:[GameHighscores] = []
        UserDefaults.standard.set(encodable: GameHighscoresArray, forKey: Keys.scoreKey.rawValue)
        GameHighscoresArrayGlobal = []
        highScoresTableView.reloadData()
    }
    

    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension HighscoresViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GameHighscoresArrayGlobal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HighscoresTableViewCell", for: indexPath) as? HighscoresTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(object: GameHighscoresArrayGlobal[indexPath.row])
        cell.number.text = String(indexPath.row + 1)
        return cell
    }

}
