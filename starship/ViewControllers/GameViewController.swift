import UIKit
import CoreMotion

enum Keys: String {
    case scoreKey = "scoreArray"
    case mySetting = "Setting"
}
class GameViewController: UIViewController {
    
    @IBOutlet weak var gameView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var livesBar: UIView!
    @IBOutlet weak var livesLabel: UILabel!
    @IBOutlet weak var scoreBar: UIView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var fuelBar: UIView!
    @IBOutlet weak var fuelAmount: UIView!
    @IBOutlet weak var fuelConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var leftBorderView: UIView!
    @IBOutlet weak var rightBorderView: UIView!
    @IBOutlet weak var starshipView: UIView!
    
    let starshipImg = UIImageView ()
    let background = UIImageView ()
    let backgroundTwo = UIImageView ()
    let leftBorder = UIImageView()
    let leftBorderTwo = UIImageView()
    let rightBorder = UIImageView()
    let rightBorderTwo = UIImageView()
    let starship = UIImageView()
    let speed: CGFloat = 120
    let fuelDecreaser:CGFloat = 10
    var enemyArray:[UIImageView] = []
    var lifeArray:[UIImageView] = []
    var lives: Int = 3
    var collision: Bool = false
    var score = 0
    
    var scoreTimer = Timer()
    var fuelTimer = Timer()
    
    var currentSetting:Settings = Settings(difficulty: 0, difficultyName: "", enemyImage: "", playersStarshipImage: "", enemysStarshipImage: "",fuelColour: "", playerName: "")
    var GameHighscoresArray:[GameHighscores] = []
    var currnetHighscore: GameHighscores = GameHighscores(playerName: "", difficultyName: "", timestamp: "", score: 0, date: "")
    
    var motionManager = CMMotionManager()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.livesLabel.text = String(lives)
        self.livesLabel.textColor = .systemPink
        self.livesLabel.textAlignment = .center
        self.livesLabel.font = UIFont (name: "SpaceAge", size: 30)
        
        scoreLabel.text = String(score)
        self.scoreLabel.textColor = .systemPink
        self.scoreLabel.textAlignment = .center
        self.scoreLabel.font = UIFont (name: "SpaceAge", size: 30)
        
        self.fuelBar.backgroundColor = .darkGray
        self.fuelAmount.backgroundColor = .green
        
        loadSetting()
        createStarShip()
        createBackground()
        createBackgroundTwo()
        createLeftBorder()
        createLeftBorderTwo()
        createRightBorder()
        createRightBorderTwo()
        moveBackground()
        spawnEnemySet()
        checkCollisionWithEnemy()
        spawnLifeSet()
        checkCollisionWithLife()
        scoreCounter()
        fuelCounterDecreaser()
        moveStarhipWithAccelerometer()

    }
    
    func createStarShip () {
        
        let starshipDimensions = 80
        guard  let enemyImage = currentSetting.playersStarshipImage else {return}
        starship.image = UIImage (named: enemyImage)
        starship.contentMode = .scaleAspectFill
        starship.frame = CGRect (x: self.gameView.frame.width / 2 - CGFloat(starshipDimensions) / 2, y: self.gameView.frame.height - 170, width: 80, height: 80)
        self.gameView.addSubview(starship)
       
    }
    
    func createBackground() {
        
        background.frame = CGRect(x: 0,
                                  y: -50,
                                  width: self.view.frame.size.width,
                                  height: self.view.frame.size.height + 200)
        background.image = UIImage(named: "background_sky")
        background.contentMode = .scaleAspectFill
        self.gameView.addSubview(background)
        gameView.bringSubviewToFront(starshipView)
        gameView.bringSubviewToFront(leftBorder)
        gameView.bringSubviewToFront(leftBorderTwo)
        gameView.bringSubviewToFront(backButton)
        gameView.bringSubviewToFront(fuelBar)
        gameView.bringSubviewToFront(livesBar)
        gameView.bringSubviewToFront(scoreBar)
        gameView.bringSubviewToFront(starship)
    }
    func createBackgroundTwo() {
        
        backgroundTwo.frame = CGRect(x: 0,
                                     y: self.background.frame.origin.y - self.backgroundTwo.frame.size.height,
                                     width: self.view.frame.size.width,
                                     height: self.view.frame.size.height + 200)
        backgroundTwo.image = UIImage(named: "background_sky")
        backgroundTwo.contentMode = .scaleAspectFill
        self.gameView.addSubview(backgroundTwo)
        gameView.bringSubviewToFront(starshipView)
        gameView.bringSubviewToFront(leftBorder)
        gameView.bringSubviewToFront(leftBorderTwo)
        gameView.bringSubviewToFront(backButton)
        gameView.bringSubviewToFront(fuelBar)
        gameView.bringSubviewToFront(livesBar)
        gameView.bringSubviewToFront(scoreBar)
        gameView.bringSubviewToFront(starship)
    }
    func createLeftBorder()  {
        
        leftBorder.frame = CGRect(x: -20,
                                  y: -10,
                                  width: 20,
                                  height: self.view.frame.size.height + 200)
        leftBorder.image = UIImage(named: "asteroids")
        leftBorder.contentMode = .scaleAspectFill
        self.gameView.addSubview(leftBorder)
    }
    func createLeftBorderTwo()  {
        
        leftBorderTwo.frame = CGRect(x: -20,
                                     y: self.leftBorder.frame.origin.y - self.leftBorderTwo.frame.size.height,
                                     width: 20,
                                     height: self.view.frame.size.height + 200)
        leftBorderTwo.image = UIImage(named: "asteroids")
        leftBorderTwo.contentMode = .scaleAspectFill
        self.gameView.addSubview(leftBorderTwo)
    }
    
    func createRightBorder()  {
        
        rightBorder.frame = CGRect(x: self.view.frame.size.width - 10,
                                   y: -10,
                                   width: self.view.frame.size.width - (self.view.frame.size.width - 20),
                                   height: self.view.frame.size.height + 200)
        rightBorder.image = UIImage(named: "asteroids")
        rightBorder.contentMode = .scaleAspectFill
        self.gameView.addSubview(rightBorder)
    }
    func createRightBorderTwo()  {
        
        rightBorderTwo.frame = CGRect(x: self.view.frame.size.width - 10,
                                      y: self.rightBorder.frame.origin.y - self.rightBorderTwo.frame.size.height,
                                      width: self.view.frame.size.width - (self.view.frame.size.width - 20),
                                      height: self.view.frame.size.height + 200)
        rightBorderTwo.image = UIImage(named: "asteroids")
        rightBorderTwo.contentMode = .scaleAspectFill
        self.gameView.addSubview(rightBorderTwo)
    }
    
    func moveBackground() {
        if self.collision == false {
            
            let moveBackground = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (moveBackground) in
                if (self.background.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.background.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                    self.backgroundTwo.frame.origin.y = self.background.frame.origin.y - self.backgroundTwo.frame.size.height
                    self.moveBackgroundElements()
                } else if (self.backgroundTwo.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.backgroundTwo.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                    self.background.frame.origin.y = self.backgroundTwo.frame.origin.y - self.background.frame.size.height
                    self.moveBackgroundElements()
                } else {
                    self.moveBackgroundElements()
                }
                if (self.leftBorder.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.leftBorder.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                    self.leftBorderTwo.frame.origin.y = self.leftBorder.frame.origin.y - self.leftBorderTwo.frame.size.height
                    self.moveBackgroundAsteroidsLeft()
                } else if (self.leftBorderTwo.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.leftBorderTwo.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                    self.leftBorder.frame.origin.y = self.leftBorderTwo.frame.origin.y - self.leftBorder.frame.size.height
                    self.moveBackgroundAsteroidsLeft()
                } else {
                    self.moveBackgroundAsteroidsLeft()
                }
                if (self.rightBorder.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.leftBorder.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                    self.rightBorderTwo.frame.origin.y = self.rightBorder.frame.origin.y - self.leftBorderTwo.frame.size.height
                    self.moveBackgroundAsteroidsRight()
                } else if (self.rightBorderTwo.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.rightBorderTwo.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                    self.rightBorder.frame.origin.y = self.rightBorderTwo.frame.origin.y - self.rightBorder.frame.size.height
                    self.moveBackgroundAsteroidsRight()
                } else {
                    self.moveBackgroundAsteroidsRight()
                }
                
            }
            )}
    }
    
    func moveBackgroundElements () {
        if self.collision == false {
            UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
                
                self.background.frame.origin.y += self.speed
                self.backgroundTwo.frame.origin.y += self.speed
                
            }
        }
    }
    func moveBackgroundAsteroidsLeft() {
        if self.collision == false {
            UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
                
                self.leftBorder.frame.origin.y += self.speed * 1.5
                self.leftBorderTwo.frame.origin.y += self.speed * 1.5
                self.collisionWithAsteroids()
            }
        }
    }
    func moveBackgroundAsteroidsRight() {
        if self.collision == false {
            UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
                
                self.rightBorder.frame.origin.y += self.speed * 1.5
                self.rightBorderTwo.frame.origin.y += self.speed * 1.5
                self.collisionWithAsteroids()
            }
        }
    }
    
    func collisionWithAsteroids () {
        if rightBorderView.frame.intersects(starship.frame) || leftBorderView.frame.intersects(starship.frame) {
            
            liveDecreasing()
        }
    }
    
    func spawnEnemyView() {
        
        let enemyView = UIImageView ()
        guard let enemyImage = currentSetting.enemysStarshipImage else {return}
        enemyView.image = UIImage (named: enemyImage)
        enemyView.contentMode = .scaleAspectFill
        enemyView.frame = CGRect (x: .random(in: 40...self.view.frame.size.width - 90), y: -100, width: 50, height: 50)
        self.gameView.addSubview(enemyView)
        self.enemyArray.append(enemyView)
        self.moveEnemy(enemy: enemyView)
        //self.collisionWithEnemy(enemy: enemyView)
        
    }
    
    func moveEnemy(enemy: UIImageView) {
        
        UIView.animate(withDuration: Double(currentSetting.difficulty!), delay: 0, options: .curveLinear) {
            enemy.frame.origin.y += self.view.frame.height + 200
        } completion: { (_) in
            enemy.removeFromSuperview()
            self.enemyArray.removeFirst()
        }
    }
    
    
    func spawnEnemySet() {
        
        let timerSpawn = Timer.scheduledTimer(withTimeInterval: .random(in: 3...7), repeats: false) { (timer) in
            
            self.spawnEnemyView()
            self.spawnEnemySet()
            
        }
    }
    
    func checkCollisionWithEnemy() {
        let intersactionTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { [self] (internalTimer) in
                for enemy in self.enemyArray {
                    if let mainPlanePresentation = starship.layer.presentation(),
                       let planePresentation = enemy.layer.presentation(),
                       mainPlanePresentation.frame.intersects(planePresentation.frame) {
                        self.liveDecreasing()
                        enemy.removeFromSuperview()
                    }
            }
        }
    }

    func spawnLifeView() {

        let lifeView = UIImageView ()

        lifeView.image = UIImage (named: "heart")
        lifeView.contentMode = .scaleAspectFill
        lifeView.frame = CGRect (x: .random(in: 40...self.view.frame.size.width - 90), y: -100, width: 50, height: 50)
        self.gameView.addSubview(lifeView)
        self.lifeArray.append(lifeView)
        self.moveLife(life: lifeView)
        //self.collisionWithEnemy(enemy: enemyView)

    }

    func moveLife(life: UIImageView) {

        UIView.animate(withDuration: Double(currentSetting.difficulty!), delay: 0, options: .curveLinear) {
            life.frame.origin.y += self.view.frame.height + 200
        } completion: { (_) in
            life.removeFromSuperview()
            self.lifeArray.removeFirst()
        }
    }


    func spawnLifeSet() {

        let timerSpawn = Timer.scheduledTimer(withTimeInterval: .random(in: 15...25), repeats: false) { (timer) in

            self.spawnLifeView()
            self.spawnLifeSet()
        }
    }

    func checkCollisionWithLife() {
        let intersactionTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { [self] (internalTimer) in
                for life in self.lifeArray {
                    if let mainPlanePresentation = starship.layer.presentation(),
                       let lifePresentation = life.layer.presentation(),
                       mainPlanePresentation.frame.intersects(lifePresentation.frame) {
                        self.lifeIncreaser()
                        life.removeFromSuperview()
                    }
            }
        }
    }

    func gameOver () {
        self.collision = true
        self.nukeAllAnimations ()
        fuelTimer.invalidate()
        scoreTimer.invalidate()
        self.saveResult()
        let gameOverController = self.storyboard?.instantiateViewController(withIdentifier: "GameOverController") as! GameOverController
        self.navigationController?.pushViewController(gameOverController, animated: false)
        //self.scoreArray = scoreArray
        //self.scoreArray.append(finalResult)
    }
    
    func saveResult () {
        
        currnetHighscore.playerName = currentSetting.playerName
        currnetHighscore.score = self.score
        
        let timestampDate = Date()
        let dateformatter = DateFormatter()
        dateformatter.timeZone = .current
        dateformatter.dateFormat = "dd MMM yyyy hh:mm:ss"
        currnetHighscore.date = dateformatter.string(from: timestampDate)
        
        currnetHighscore.difficultyName = currentSetting.difficultyName
        
        var GameHighscoresArray = UserDefaults.standard.value([GameHighscores].self, forKey: Keys.scoreKey.rawValue) ?? []
        GameHighscoresArray.append(currnetHighscore)
        UserDefaults.standard.set(encodable: GameHighscoresArray, forKey: Keys.scoreKey.rawValue)
  
    }
    
    func nukeAllAnimations() {
        self.gameView.subviews.forEach({$0.layer.removeAllAnimations()})
        self.gameView.layer.removeAllAnimations()
        self.gameView.layoutIfNeeded()
    }
    
    func liveDecreasing () {
        if lives == 0 {
            gameOver()
        } else {
            lives -= 1
        }
        self.livesLabel.text = String(lives)
        var starshipWidth = starshipView.frame.width
        starship.frame.origin.x = gameView.frame.width / 2 - starshipWidth / 2
    }
    
    func lifeIncreaser () {
        lives += 1
        self.livesLabel.text = String(lives)
    }
    
    func scoreCounter () {
        

        scoreTimer = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: true) { (scoreTimer) in
            self.score += 1
            self.scoreLabel.text = String(self.score)
        
        }
    }
    
    func fuelCounterDecreaser () {
        
        
        fuelTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (fuelTimer) in
            if self.fuelAmount.frame.width > 0 {
                self.fuelConstraint.constant += 10
                UIView.animate(withDuration: 1) {
                    self.view.layoutIfNeeded()
                }
            
            } else {
                self.liveDecreasing()
                self.fuelConstraint.constant = 0
            }
        }
    }
    func loadSetting() {
        guard let Setting = UserDefaults.standard.value(Settings.self, forKey: Keys.mySetting.rawValue) else { return }
        currentSetting = Setting
    }
    
    func moveStarhipWithAccelerometer () {
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = 0.01
            motionManager.startAccelerometerUpdates(to: .main) { [weak self] (data: CMAccelerometerData?, error: Error?) in
                if let acceleration = data?.acceleration {
                    if acceleration.x < -0.2 {
                        self!.starship.frame.origin.x -= 2
                    } else if acceleration.x > 0.2 {
                        self!.starship.frame.origin.x += 2
                    }
                }
            }
        }
    }
    
    @IBAction func rightButton(_ sender: UIButton) {
        self.starship.frame.origin.x -= 10
    }
    @IBAction func leftButton(_ sender: UIButton) {
        self.starship.frame.origin.x += 10
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
}
