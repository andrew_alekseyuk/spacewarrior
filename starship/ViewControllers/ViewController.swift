import UIKit

class ViewController: UIViewController {
    

    
    @IBOutlet weak var gameName: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var highscoresButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    let background = UIImageView ()
    let backgroundTwo = UIImageView ()
    let leftBorder = UIImageView()
    let leftBorderTwo = UIImageView()
    let rightBorder = UIImageView()
    let rightBorderTwo = UIImageView()
    let speed: CGFloat = 30
    
    let gameNameText = "Space \n warrior"
    
    var settings = Settings(difficulty: 7, difficultyName: "Normal", enemyImage: "enemy", playersStarshipImage: "starship", enemysStarshipImage: "enemy", fuelColour: "green", playerName: "")
    
    @IBOutlet weak var backgroundMenu: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        gameName.text = self.gameNameText
        gameName.font = UIFont(name: "SpaceAge", size: 50)
        gameName.textColor = .systemPink
        gameName.textAlignment = .center
        
        createBackground()
        createBackgroundTwo()
        moveBackground()

        
        self.startButton.cornerRadius()
        self.startButton.dropShadow()
        self.highscoresButton.cornerRadius()
        self.highscoresButton.dropShadow()
        self.settingsButton.cornerRadius()
        self.settingsButton.dropShadow()
        startButton.setTitle("Start", for: .normal)
        startButton.titleLabel?.font = UIFont (name: "SpaceAge", size: 35)
    
        settingsButton.setTitle("Settings", for: .normal)
        settingsButton.titleLabel?.font = UIFont (name: "SpaceAge", size: 35)
        
        highscoresButton.setTitle("Highscores", for: .normal)
        highscoresButton.titleLabel?.font = UIFont (name: "SpaceAge", size: 35)
        
        
        
        
        guard let setting = UserDefaults.standard.value(Settings.self, forKey: Keys.mySetting.rawValue) else { return }
        if setting.difficulty == nil {
            let defaultSettings = settings
            UserDefaults.standard.set(encodable: defaultSettings, forKey: Keys.mySetting.rawValue)
        } else {
            settings = setting
        }
        
        
        
    }
    func createBackground() {
        
        background.frame = CGRect(x: 0,
                                  y: -50,
                                  width: self.view.frame.size.width,
                                  height: self.view.frame.size.height + 200)
        background.image = UIImage(named: "background_sky")
        background.contentMode = .scaleAspectFill
        self.backgroundMenu.addSubview(background)
        
        view.bringSubviewToFront(self.startButton)
        view.bringSubviewToFront(leftBorderTwo)
    }
    func createBackgroundTwo() {
        
        backgroundTwo.frame = CGRect(x: 0,
                                     y: self.background.frame.origin.y - self.backgroundTwo.frame.size.height,
                                     width: self.view.frame.size.width,
                                     height: self.view.frame.size.height + 200)
        backgroundTwo.image = UIImage(named: "background_sky")
        backgroundTwo.contentMode = .scaleAspectFill
        self.backgroundMenu.addSubview(backgroundTwo)
        
        view.bringSubviewToFront(self.startButton)
        view.bringSubviewToFront(leftBorderTwo)
    }
    
    
    func moveBackground() {
        let moveBackground = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (moveBackground) in
            if (self.background.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.background.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                self.backgroundTwo.frame.origin.y = self.background.frame.origin.y - self.backgroundTwo.frame.size.height
                self.moveBackgroundElements()
            } else if (self.backgroundTwo.layer.presentation()?.frame.origin.y)! >= -2 * self.speed && (self.backgroundTwo.layer.presentation()?.frame.origin.y)! < self.view.frame.size.height {
                self.background.frame.origin.y = self.backgroundTwo.frame.origin.y - self.background.frame.size.height
                self.moveBackgroundElements()
            } else {
                self.moveBackgroundElements()
            }
            
            
        }
        )}
    
    func moveBackgroundElements () {
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
            
            self.background.frame.origin.y += self.speed
            self.backgroundTwo.frame.origin.y += self.speed
            
        }
    }
    


    @IBAction func startGameButton(_ sender: UIButton) {
        let gameController = self.storyboard?.instantiateViewController(withIdentifier: "GameViewController") as! GameViewController
        //self.present(gameController, animated: true, completion: nil)
        guard let setting = UserDefaults.standard.value(Settings.self, forKey: Keys.mySetting.rawValue) else { return }
        self.settings = setting
        if self.settings.playerName == "" {
            let alert = UIAlertController(title: "Hi", message: "Enter your name", preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.text = ""
            }
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
            }))
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                self.settings.playerName = textField?.text
                UserDefaults.standard.set(encodable: self.settings, forKey: Keys.mySetting.rawValue)
                self.navigationController?.pushViewController(gameController, animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        self.navigationController?.pushViewController(gameController, animated: true)
        
        
        
    }
    @IBAction func settingsButton(_ sender: UIButton) {
        let settingsController = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        //self.present(settingsController, animated: true, completion: nil)
        self.navigationController?.pushViewController(settingsController, animated: true)
    }
    @IBAction func highscoresButton(_ sender: UIButton) {
        let highscoresController = self.storyboard?.instantiateViewController(withIdentifier: "HighscoresViewController") as! HighscoresViewController
        //self.present(highscoresController, animated: true, completion: nil)
        self.navigationController?.pushViewController(highscoresController, animated: true)
    }
    
    
}


